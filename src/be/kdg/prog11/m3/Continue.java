package be.kdg.prog11.m3;

/**
 * Author: derijkej
 */
public class Continue {

	public static void main(String[] args) {
		int counter = 0;
		while (++counter < 10) {
			if (counter % 3 == 0) {continue;}
			System.out.print(counter + " ");
		}
		// 1 2 4 5 7 8
	}
}
