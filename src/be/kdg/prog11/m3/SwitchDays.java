package be.kdg.prog11.m3;

import java.util.Scanner;

public class SwitchDays {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter the number of the day: ");
		int day = keyboard.nextInt();
		System.out.print("It is ");
		switch (day) {
			case 1:
				System.out.println("monday");
				break;
			case 2:
				System.out.println("tuesday");
				break;
			case 3:
				System.out.println("wednesday");
				break;
			case 4:
				System.out.println("thursday");
				break;
			case 5:
				System.out.println("friday");
				break;
			case 6:
				System.out.println("saturday");
				break;
			default:
				System.out.println("sunday");
		}
	}
}
