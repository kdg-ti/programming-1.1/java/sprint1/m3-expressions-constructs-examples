package be.kdg.prog11.m3;

import java.util.Scanner;

/**
 * Author: derijkej
 */
public class HowMany {
	public static void main(String[] args) {
		int guess;
		int secret = 56;
		int counter = 0;

		Scanner keyboard = new Scanner(System.in);
		while (true) {
			System.out.print("Enter a number: ");
			guess = keyboard.nextInt();
			counter++;
			if (guess == secret) {
				System.out.print("Congratulations, you used " + counter
					+ " guesses to find the right number!");
				return;
			} //end if
//			if (guess < secret) System.out.print("Higher! ");
//			else  System.out.print("Lower! ");
			System.out.println(guess < secret ? "Higher! ":"Lower! ");
		} //end while
	}//end main
}
