package be.kdg.prog11.m3;

/**
 * Author: derijkej
 */
public class Scope {
	public static void main(String[] args) {
		boolean bool = false;
		int outer = 100;
		if (!bool) {
			int inner = 10;
			System.out.println(outer + inner);
		}
		bool = true;
		if (bool) {
			int inner = 42;
			System.out.println(outer + inner);
		}
		System.out.println(outer);
	}
}