package be.kdg.prog11.m3;

public class Switch {

	public static void main(String[] args) {
		printDay(3);
		printSeason("october");
		printSeasonSimplified("december");
		printSeasonExpression("march");
		printSeasonYield("maart");
	}

	private static void printSeasonSimplified(String month) {
			switch (month) {
				case "march", "april", "may" -> System.out.println("spring");
				case "june", "july", "august" -> System.out.println("summer");
				case "september", "october", "november" -> System.out.println("autumn");
				case "december", "january", "february" -> System.out.println("winter");
				default -> System.out.println("invalid month");
			}
	}

	private static void printSeasonExpression(String month) {
		String seasonName = switch (month) {
			case "march", "april", "may" -> "spring";
			case "june", "july", "august" -> "summer";
			case "september", "october", "november" -> "autumn";
			case "december", "january", "february" -> "winter";
			default -> {
				System.out.println(">>> Please use lower case english month names");
				yield "invalid month";}
		};
		System.out.println(seasonName);
	}



	private static void printSeasonYield(String month) {
		String seasonName = switch (month) {
			case "march", "april", "may" -> "spring";
			case "june", "july", "august" -> "summer";
			case "september", "october", "november" -> "autumn";
			case "december", "january", "february" -> "winter";
			default -> {
				System.out.println("Please use lower case english month names ");
				yield "invalid month";}
		};
		System.out.println(seasonName);
	}

	private static void printSeason(String month) {
		switch (month) {
			case "march":
			case "april":
			case "may":
				System.out.println("spring");
				break;
			case "june":
			case "july":
			case "august":
				System.out.println("summer");
				break;
			case "september":
			case "october":
			case "november":
				System.out.println("autumn");
				break;
			case "december":
			case "january":
			case "february":
				System.out.println("winter");
				break;
			default:
				System.out.println("invalid month");
		}

	}

	public static void printDay(int day) {
		switch (day) {
			case 1:
				System.out.println("monday");
				break;
			case 2:
				System.out.println("tuesday");
				break;
			case 3:
				System.out.println("wednesday");
				break;
			case 4:
				System.out.println("thursday");
				break;
			case 5:
				System.out.println("friday");
				break;
			case 6:
				System.out.println("saturday");
				break;
			case 0:
				System.out.println("sunday");
				break;
		}
	}


}
